# Distributed Miner
This application will search for a nonce that gives a hash value less than a specified target. It uses worker processes distributed across different machines working together to find a solution.

## Running the Application
```
docker-compose up --build
```

You should see output like this once the nodes are up:
```
worker3_1  | Waiting for a solution to be found...
worker2_1  | Waiting for a solution to be found...
worker3_1  | Listening on UDP port 26425...
worker2_1  | Listening on UDP port 26425...
worker1_1  | Waiting for a solution to be found...
worker1_1  | Listening on UDP port 26425...
worker3_1  | Sent solution packet to worker1:26425
worker1_1  | Received packet
worker1_1  | Found Solution!
worker1_1  | Nonce: 13822127
worker1_1  | Hash: 000000ADC39503215B23AE9459B200AA4EFECDD772D12E444CC73CC9F554B60D
worker1_1  | Hash value: 4684667970367382772012728813558791727998969551240734792598505194305037
worker3_1  | Sent solution packet to worker2:26425
worker2_1  | Received packet
worker2_1  | Found Solution!
worker2_1  | Nonce: 13822127
worker3_1  | Sent solution packet to localhost:26425
worker3_1  | Received packet
worker3_1  | Found Solution!
worker3_1  | Nonce: 13822127
worker3_1  | Hash: 000000ADC39503215B23AE9459B200AA4EFECDD772D12E444CC73CC9F554B60D
worker3_1  | Hash value: 4684667970367382772012728813558791727998969551240734792598505194305037
worker2_1  | Hash: 000000ADC39503215B23AE9459B200AA4EFECDD772D12E444CC73CC9F554B60D
worker2_1  | Hash value: 4684667970367382772012728813558791727998969551240734792598505194305037
groupproject1_worker3_1 exited with code 0
groupproject1_worker2_1 exited with code 0
groupproject1_worker1_1 exited with code 0

```