package edu.rit.cs;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Main {

    private static int threadCount;
    private static int workerCount;
    private static int workerIndex;
    private static List<String> workerHosts;

    /**
     * Read all of the settings necessary for configuring the application in the environment
     */
    private static void readSettingsFromEnvironment() {
        Map<String, String> envVariables = System.getenv();

        threadCount = Integer.parseInt(envVariables.get("MINER_THREAD_COUNT"));
        workerCount = Integer.parseInt(envVariables.get("MINER_WORKER_COUNT"));
        workerIndex = Integer.parseInt(envVariables.get("MINER_WORKER_INDEX"));

        String workerHostList = envVariables.get("MINER_WORKER_HOSTS");
        if (workerHostList.length() == 0) {
            workerHosts = new ArrayList<>();
        } else {
            workerHosts = new ArrayList<>(Arrays.asList(workerHostList.split(";")));
        }
    }

    /**
     * Application entry point
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        readSettingsFromEnvironment();
        //ResultListener listener = new ResultListenerLocal();

        ResultListener listener = new ResultListenerNetwork(workerHosts);
        Thread listenerThread = new Thread(listener);
        listenerThread.start();

        byte[] targetHash = {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            int multiple = threadCount * workerCount;
            int offset = workerIndex * threadCount + i;

            Miner m = new Miner(new BigInteger(targetHash), multiple, offset, listener);
            Thread t = new Thread(m);
            t.start();

            threads.add(t);
        }

        try {
            for (Thread t : threads) {
                t.join();
            }
        } catch (InterruptedException ex) {
            System.out.println("Interrupted");
        }
    }
}
