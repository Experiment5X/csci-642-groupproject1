package edu.rit.cs;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.math.BigInteger;

/**
 * Search for a nonce whose block hash yields a value less than the target
 */
public class Miner implements Runnable {
    private final long multiple;
    private final long offset;
    private final BigInteger target;
    private final ResultListener listener;

    /**
     * Miner constructor
     *
     * @param target The target value for the hash to be under
     * @param multiple The multiple to go by when trying nonce values
     * @param offset The offset to start at when trying nonce values
     * @param listener The object responsible for keeping track of whether or not the solution
     *                 has been found.
     */
    public Miner(BigInteger target, long multiple, long offset, ResultListener listener)
    {
        this.target = target;
        this.multiple = multiple;
        this.offset = offset;
        this.listener = listener;
    }

    /**
     * Get the bytes for a block with a specified nonce
     *
     * @param nonce The nonce value to put in the block
     * @return The bytes of the block
     */
    private byte[] getBlockBuffer(long nonce) {
        String message = "I like turtles";

        ByteBuffer bufferBuilder = ByteBuffer.allocate(Long.BYTES + message.getBytes().length * 2);
        bufferBuilder.putLong(nonce);

        for (char c : message.toCharArray()) {
            bufferBuilder.putChar(c);
        }

        return bufferBuilder.array();
    }

    /**
     * Calculate the SHA-256 hash
     *
     * @param buffer The bytes to calculate the hash of
     * @return The hash
     */
    private byte[] sha256(byte[] buffer) {
        try {
            MessageDigest shaHasher = MessageDigest.getInstance("SHA-256");
            shaHasher.update(buffer);
            byte[] hash = shaHasher.digest();

            return hash;
        } catch (NoSuchAlgorithmException ex) {
        }

        return null;
    }

    /**
     * Perform two rounds of SHA256
     *
     * @param buffer The bytes to take the hash of twice
     * @return The hash
     */
    private byte[] doubleRoundSha256(byte[] buffer) {
        byte[] hash = sha256(sha256(buffer));

        if (hash == null) {
            return null;
        } else {
            return hash;
        }
    }

    /**
     * Check if the given nonce yields a hash below the target value
     *
     * @param nonce The nonce to try
     * @param hash The hash of the block with the nonce
     * @return True if the hash is below the target, otherwise false
     */
    private boolean checkForSolution(long nonce, byte[] hash) {
        BigInteger hashValue = new BigInteger(1, hash);

        if (hashValue.compareTo(this.target) < 0) {
            synchronized (listener) {
                if (!listener.isSolved()) {
                    listener.setNonce(nonce);
                    listener.setHashBuffer(hash);
                    listener.setHashValue(hashValue);
                    listener.notifySolutionFound();
                }
            }
            return true;
        }

        return false;
    }

    /**
     * Try nonce values searching for one that gives a hash below the target
     */
    private void findHash() {
       for (long i = this.offset; i < Long.MAX_VALUE; i += this.multiple) {
           // check to see if another thread found a solution
           if (listener.isSolved()) {
               return;
           }

           byte[] block = getBlockBuffer(i);
           byte[] blockHash = doubleRoundSha256(block);

           if (this.checkForSolution(i, blockHash)) {
               return;
           }
       }
    }

    /**
     * Look for a nonce
     */
    @Override
    public void run() {
        findHash();
    }
}
