package edu.rit.cs;

// may not be necessary
public class ResultListenerLocal extends ResultListener {

    @Override
    public void notifySolutionFound() {
        synchronized (this) {
            this.notify();
        }
    }

    @Override
    protected void listenForSolution() {

        synchronized (this) {
            try {
                this.wait();

                this.printSolution();
            } catch (InterruptedException ex) {

            }
        }
    }
}
