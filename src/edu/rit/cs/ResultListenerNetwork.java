package edu.rit.cs;

import java.io.IOException;
import java.math.BigInteger;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.List;

/**
 * Communicate with other workers over UDP for their solutions
 */
public class ResultListenerNetwork extends ResultListener {
    private final static int PORT = 26425;

    private List<String> listenerHostnames;
    private DatagramSocket socket;

    /**
     * Create a UDP result listener
     *
     * @param listenerHostnames The hostnames of the other workers to communicate with
     */
    public ResultListenerNetwork(List<String> listenerHostnames) {
        try {
            this.listenerHostnames = listenerHostnames;
            this.listenerHostnames.add("localhost");

            this.socket = new DatagramSocket(PORT);
        } catch (SocketException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Send a UDP packet to all the other workers with the solution, including the
     * nonce and the hash.
     */
    @Override
    public void notifySolutionFound() {
        ByteBuffer bufferBuilder = ByteBuffer.allocate(40);
        bufferBuilder.putLong(this.nonce);
        bufferBuilder.put(this.hashBuffer);
        byte[] packetData = bufferBuilder.array();

        for (String host : this.listenerHostnames) {
            try {
                InetAddress ipv4Address = Inet4Address.getByName(host);

                DatagramPacket packet = new DatagramPacket(packetData, packetData.length);
                packet.setAddress(ipv4Address);
                packet.setPort(PORT);

                System.out.printf("Sent solution packet to %s:%d\n", host, PORT);
                this.socket.send(packet);

            } catch (UnknownHostException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Listen for other workers to notify us with their solution.
     */
    @Override
    protected void listenForSolution() {
        try {
            byte[] buffer = new byte[40];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            System.out.printf("Listening on UDP port %d...\n", PORT);
            socket.receive(packet);
            System.out.println("Received packet");

            ByteBuffer buffWrapper = ByteBuffer.wrap(buffer);
            this.nonce = buffWrapper.getLong();

            this.hashBuffer = new byte[32];
            buffWrapper.get(this.hashBuffer, 0, 32);
            this.hashValue = new BigInteger(1, this.hashBuffer);

            this.isSolved = true;

            this.printSolution();

        } catch (SocketException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
