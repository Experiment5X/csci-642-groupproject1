package edu.rit.cs;

import java.math.BigInteger;

/**
 * Encapsulates a solution and listens for workers to have found one
 */
public abstract class ResultListener implements Runnable {
    protected boolean isSolved = false;
    protected long nonce;
    protected byte[] hashBuffer;
    protected BigInteger hashValue;

    /**
     * Notify all the workers of the solution
     */
    public abstract void notifySolutionFound();

    /**
     * Listen for other workers to tell us about their solution
     */
    protected abstract void listenForSolution();

    @Override
    public void run() {
        System.out.println("Waiting for a solution to be found...");
        this.listenForSolution();
    }

    // copied from: https://stackoverflow.com/a/9855338
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Display the solution to the user
     */
    protected void printSolution() {
        System.out.println("Found Solution!");
        System.out.printf("Nonce: %d\n", this.getNonce());
        System.out.println("Hash: " + bytesToHex(this.hashBuffer));
        System.out.println("Hash value: " + this.getHashValue());
    }

    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
        this.isSolved = true;
    }

    public BigInteger getHashValue() {
        return hashValue;
    }

    public void setHashValue(BigInteger hashValue) {
        this.hashValue = hashValue;
    }

    public byte[] getHashBuffer() {
        return hashBuffer;
    }

    public void setHashBuffer(byte[] hashBuffer) {
        this.hashBuffer = hashBuffer;
    }

    /**
     * Check if a nonce has been found
     * @return True if nonce found, otherwise false
     */
    public boolean isSolved() {
        return isSolved;
    }

}
