FROM peiworld/csci652

COPY ./src /code

RUN mkdir /out
RUN javac -d /out /code/edu/rit/cs/*

CMD cd /out && java edu.rit.cs.Main